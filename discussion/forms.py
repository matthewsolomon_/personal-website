from django import forms
from .models import Comment, Post
from mptt.forms import TreeNodeChoiceField


class NewCommentForm(forms.ModelForm):
    parent = TreeNodeChoiceField(queryset=Comment.objects.all())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['parent'].widget.attrs.update(
            {'class': 'd-none'})
        self.fields['parent'].label = ''
        self.fields['parent'].required = False

    class Meta:
        model = Comment
        fields = ('name', 'parent', 'email', 'content')

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control mb-3', 'placeholder': 'Name'}),
            'email': forms.TextInput(attrs={'class': 'form-control mb-3', 'placeholder': 'E-mail'}),
            'content': forms.Textarea(attrs={'class': 'form-control mb-3', 'placeholder': 'Content'}),
        }

        labels = {
            'name': 'Name',
            'email': "E-mail",
            'content': 'Content',
        }

    def save(self, *args, **kwargs):
        Comment.objects.rebuild()
        return super(NewCommentForm, self).save(*args, **kwargs)

class PostForm(forms.ModelForm):
    title = forms.CharField(label="Judul",
                           widget=forms.TextInput(
                               attrs={
                                   "placeholder": "Judul",
                                   "class": "form-control",
                               }
                           )
                           )
    author = forms.CharField(label="Nama",
                           widget=forms.TextInput(
                               attrs={
                                   "placeholder": "Nama",
                                   "class": "form-control",
                               }
                           )
                           )
    excerpt = forms.CharField(label="Kelompok",
                           widget=forms.TextInput(
                               attrs={
                                   "placeholder": "Kelompok",
                                   "class": "form-control",
                               }
                           )
                           )
    content = forms.CharField(label="Pertanyaan",
                           widget=forms.Textarea(
                               attrs={
                                   "placeholder": "Pertanyaan",
                                   "class": "form-control",
                               }
                           )
                           )
    class Meta:
        model = Post
        fields = [
            'title',
            'author',
            'excerpt',
            'content',
        ]