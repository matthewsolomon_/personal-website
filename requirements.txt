asgiref==3.2.10
coverage==5.3
dj-database-url==0.5.0
Django==3.1.2
django-js-asset==1.2.2
django-mptt==0.11.0
gunicorn==20.0.4
Pillow==8.0.1
psycopg2==2.8.6
pycodestyle==2.6.0
pytz==2020.1
selenium==3.141.0
sqlparse==0.4.1
toml==0.10.2
txt==2020.11.12
urllib3==1.26.2
whitenoise==5.2.0
